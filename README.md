# README #

This game was created for the GDS6 Game Jam, the theme was "Second Chance" (Either a game that involves the player getting a second chance, or the developer having a second chance to start a project). 
Due to the nature of game jams, the quality of the code is very poor. 
I currently do not have any plans to continue this project but if anybody is interested in picking it up, e-mail me.

### Setup ###

* This project uses libGDX with the Intellij IDEA. You can download the free community edition of Intellij IDEA [here](http://www.jetbrains.com/idea/download/).
* For details on how import the project into Intellij IDEA, please refer to [this wiki page](https://github.com/libgdx/libgdx/wiki/Gradle-and-Intellij-IDEA)

### Credits ###

* Adam Docherty - Programmer (adam.p.docherty@gmail.com)
* Art was taken from [Muramasa: The Demon Blade](http://en.wikipedia.org/wiki/Muramasa:_The_Demon_Blade), and [Open Game Art](http://opengameart.org/).
* Music was taken from the [Kara no Kyoukai OST](https://www.youtube.com/watch?v=T0e7fnSCGus). Sounds were taken from [Freesound](http://freesound.org/).

# Gameplay #

Gameplay demo video can be found [here](https://www.youtube.com/watch?v=KH0D_Ds6Z9M).