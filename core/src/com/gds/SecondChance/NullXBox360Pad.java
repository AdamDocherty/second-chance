package com.gds.SecondChance;

import com.badlogic.gdx.controllers.Controller;

/**
 * Created by Adam on 05/09/2014.
 */
public class NullXBox360Pad extends XBox360Pad {

    @Override
    public void update() {

    }

    @Override
    public boolean isButtonPressed(int buttonId) {
        return false;
    }

    @Override
    public boolean isButtonJustPressed(int buttonId) {
        return false;
    }

    @Override
    public float getAxis(int axisId) {
        return 0.0f;
    }

    public NullXBox360Pad() {
        super(null);
    }
}
