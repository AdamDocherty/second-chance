package com.gds.SecondChance.GameObjects.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.gds.SecondChance.GameObjects.GameObject;
import com.gds.SecondChance.GameObjects.Lamp;
import com.gds.SecondChance.GameObjects.Spectre;
import com.gds.SecondChance.Map;
import com.gds.SecondChance.NullXBox360Pad;
import com.gds.SecondChance.XBox360Pad;

/**
 * The player class.
 * Created by Adam on 01/09/2014.
 */
public class Player extends GameObject {
    public enum State {
        IDLE,
        RUN,
        JUMP,
        SPAWN,
        DYING,
        DEAD
    }

    public static class Direction {
        public static final int LEFT = -1;
        public static final int RIGHT = 1;
    }

    private static final float SWORD_SWING_TIME = 0.1f;

    private static final float c_acceleration = 100.0f;
    private static final float c_jumpVelocity = 16.0f;
    private static final float c_gravity = 45.0f;
    private static final float c_maxVelocity = 7.0f;
    private static final float c_sprintVelocity = 11.0f;
    private static final float c_friction = 0.5f;

    private XBox360Pad m_controller;

    //Vector2 m_position = new Vector2();
    Vector2 m_acceleration = new Vector2();
    Vector2 m_velocity = new Vector2();

    State m_state = State.SPAWN;
    float m_stateTime = 0.0f;
    int m_direction = Direction.LEFT;
    boolean m_grounded = false;
    boolean m_doubleJump = false;

    boolean m_ghost = false;

    public Corpse m_corpse = null;
    public SwordSwing m_swordSwing;

    public State getState() { return m_state; }
    public int getDirection() { return m_direction; }
    public boolean isGhost() { return m_ghost; }
    public float getStateTime() { return m_stateTime; }

    public Player(Map map, float x, float y) {
        super(map);
//        m_position.x = x;
//        m_position.y = y;
        m_rect.width = 0.6f;
        m_rect.height = 1.8f;
        setPosition(x, y);
//        m_rect.x = m_position.x + 0.2f;
//        m_rect.y = m_position.y;
        m_state = State.SPAWN;
        m_stateTime = 0.0f;

        m_swordSwing = new SwordSwing(map, this);

        if (Controllers.getControllers().size > 0) {
            m_controller = new XBox360Pad(Controllers.getControllers().get(0));
        } else {
            m_controller = new NullXBox360Pad();
        }
    }

    public void update(float deltaTime) {
        m_controller.update();
        processKeys();

        move(deltaTime);

        if (m_ghost) {
            updateGhost(deltaTime);
        } else {
            updateLiving(deltaTime);
        }

        m_swordSwing.update(deltaTime);

        if (m_state == State.SPAWN) {
            if (m_stateTime > 0.4f) {
                m_state = State.IDLE;
            }
        }

        if (m_state == State.DYING) {
//            if (m_aiStateTime > 0.4f) {
            onDeath();
//            }
        }

        m_stateTime += deltaTime;
    }

    private void move(float deltaTime) {
        m_acceleration.y = -c_gravity;
        m_acceleration.scl(deltaTime);
        m_velocity.add(m_acceleration.x, m_acceleration.y);
        if (m_acceleration.x == 0.0f) m_velocity.x *= c_friction;
        if (m_velocity.x > getMaxVelocity()) m_velocity.x = getMaxVelocity();
        if (m_velocity.x < -getMaxVelocity()) m_velocity.x = -getMaxVelocity();
        m_velocity.scl(deltaTime);
        tryMove();
        m_velocity.scl(1.0f / deltaTime);
    }

    private float getMaxVelocity() {
        boolean sprintPressed = Gdx.input.isButtonPressed(Input.Keys.SHIFT_LEFT)
                || m_controller.isButtonPressed(XBox360Pad.BUTTON_RB);
        return (sprintPressed) ? c_sprintVelocity : c_maxVelocity;
    }

    private void processKeys() {
        if (m_state == State.SPAWN || m_state == State.DYING) return;

        boolean jumpPressed = Gdx.input.isKeyJustPressed(Input.Keys.W)
                || m_controller.isButtonJustPressed(XBox360Pad.BUTTON_A);
        boolean leftPressed = Gdx.input.isKeyPressed(Input.Keys.A)
                || m_controller.getAxis(XBox360Pad.AXIS_LEFT_X) < -0.1f;
        boolean rightPressed = Gdx.input.isKeyPressed(Input.Keys.D)
                || m_controller.getAxis(XBox360Pad.AXIS_LEFT_X) > 0.1f;

        if (jumpPressed/*&& m_state != State.JUMP*/) {
            if (m_grounded) {
                m_state = State.JUMP;
                m_velocity.y = c_jumpVelocity;
                m_grounded = false;
            } else if (m_doubleJump) {
                m_velocity.y = c_jumpVelocity;
                m_doubleJump = false;
            }
        }

        if (leftPressed) {
            if (m_state != State.JUMP) m_state = State.RUN;
            m_direction = Direction.LEFT;
            m_acceleration.x = c_acceleration * m_direction;
        } else if (rightPressed) {
            if (m_state != State.JUMP) m_state = State.RUN;
            m_direction = Direction.RIGHT;
            m_acceleration.x = c_acceleration * m_direction;
        } else {
            if (m_state != State.JUMP) m_state = State.IDLE;
            m_acceleration.x = 0.0f;
        }
    }

    // Used to store the collidable rectangles
    static Rectangle[] r = {new Rectangle(), new Rectangle(),
            new Rectangle(), new Rectangle(),
            new Rectangle(), new Rectangle()};

    private void tryMove() {
        m_rect.x += m_velocity.x;
        fetchCollidableRects();
        for (Rectangle rect : r) {
            if (m_rect.overlaps(rect)) {
                if (m_velocity.x < 0.0f)
                    m_rect.x = rect.x + rect.width + 0.01f;
                else
                    m_rect.x = rect.x - m_rect.width - 0.01f;
                m_velocity.x = 0.0f;
            }
        }

        m_rect.y += m_velocity.y;
        m_grounded = false;
        fetchCollidableRects();
        for (Rectangle rect : r) {
            if (m_rect.overlaps(rect)) {
                if (m_velocity.y < 0.0f) {
                    m_rect.y = rect.y + rect.height + 0.001f;
                    m_grounded = true;
                    m_doubleJump = true;
                    if (m_state != State.DYING && m_state != State.SPAWN)
                        m_state = Math.abs(m_acceleration.x) > 0.1f ? State.RUN : State.IDLE;
                } else {
                    m_rect.y = rect.y - m_rect.height - 0.01f;
                }
                m_velocity.y = 0.0f;
            }
        }

//        m_position.x = m_rect.x - 0.2f;
//        m_position.y = m_rect.y;
    }

    private void fetchCollidableRects() {

        int[] px = new int[6];
        int[] py =  new int[6];
        int[] tiles = new int[6];

        px[0] = (int)m_rect.x;
        py[0] = (int)Math.floor(m_rect.y);
        px[1] = (int)(m_rect.x + m_rect.width);
        py[1] = (int)Math.floor(m_rect.y);
        px[2] = (int)(m_rect.x + m_rect.width);
        py[2] = (int)(m_rect.y + 1);
        px[3] = (int)m_rect.x;
        py[3] = (int)(m_rect.y + 1);
        px[4] = (int)(m_rect.x + m_rect.width);
        py[4] = (int)(m_rect.y + m_rect.height);
        px[5] = (int)m_rect.x;
        py[5] = (int)(m_rect.y + m_rect.height);

        for (int i = 0; i < 6; ++i) {
            tiles[i] = m_map.getTile(px[i], m_map.height() - 1 - py[i]);
        }

        if (!m_ghost) {
            if (m_state != State.DYING) {
                for (int tile : tiles) {
                    if (m_map.isDeadly(tile)) {
                        m_state = State.DYING;
                        m_stateTime = 0;
                    }
                }
            }

            for (int tile : tiles) {
                if (m_map.isEnd(tile)) {
                    levelComplete();
                }
            }
        }

        for (int i = 0; i < 6 ; ++i) {
            if (m_map.isCollidable(tiles[i], m_ghost))
                r[i].set(px[i], py[i], 1, 1);
            else
                r[i].set(-1, -1, 0, 0);
        }
    }

    private void levelComplete() {
        m_map.onLevelComplete();
    }

    private void updateGhost(float deltaTime) {
        boolean resurrectPressed = Gdx.input.isKeyPressed(Input.Keys.R)
                || m_controller.isButtonJustPressed(XBox360Pad.BUTTON_Y);

        if (resurrectPressed) {
            resurrect();
        }

        for (Lamp lamp : m_map.m_lamps) {
            if (lamp.getState() == Lamp.State.UNLIT && lamp.getHitbox().overlaps(m_rect)) lamp.activate();
        }

        for (Spectre s : m_map.m_spectres) {
            if (!s.isActive()) continue;
            if (s.getHitbox().overlaps(m_rect)) {
                onDeath();
            }
        }
    }

    private void resurrect() {
        // cannot resurrect in mid air
        if (!m_grounded) {
            return;
        }
        Vector2 position = new Vector2(m_rect.x, m_rect.y);
        if (position.dst(m_corpse.getHitbox().x, m_corpse.getHitbox().y) < 5.0f) {
            m_ghost = false;

            if (m_map.m_boss != null) {
                if (m_map.m_boss.isColliding(this)) {
                    m_ghost = true;
                    m_state = State.IDLE;
                    return;
                }
            }

            fetchCollidableRects();
            if (m_state == State.DYING) {
                m_ghost = true;
                m_state = State.IDLE;
                return;
            }
            for (Rectangle rect : r) {
                if (rect.overlaps(m_rect)) {
                    m_ghost = true;
                    return;
                }
            }

            m_corpse = null;
        }
    }

    private void updateLiving(float deltaTime) {
        boolean attackPressed = Gdx.input.isKeyJustPressed(Input.Keys.SPACE)
                || m_controller.isButtonJustPressed(XBox360Pad.BUTTON_X);

        if (attackPressed) {
            m_swordSwing.attack(SWORD_SWING_TIME);
            for (Spectre s : m_map.m_spectres) {
                if (!s.isActive()) continue;
                if (s.getHitbox().overlaps(m_swordSwing.getHitbox())) {
                    s.hitBySword();
                }
            }
            if (m_map.m_boss != null) {
                if (m_map.m_boss.isColliding(m_swordSwing)) {
                    m_map.m_boss.onHit();
                }
            }
        }

        if (m_map.m_boss != null) {
            if (m_map.m_boss.isColliding(this)) {
                onDeath();
            }
        }
    }

    private void onDeath() {
        if (!m_ghost) {
            Sound sfx = m_map.m_assetManager.get("sfx/player-death.ogg", Sound.class);
            sfx.play();
            m_ghost = true;
            m_state = State.IDLE;
            m_corpse = new Corpse(m_map, m_rect.x, m_rect.y);
            //setPosition(m_map.getPlayerSpawnPosition());
        } else {
            // TODO: End Game
            m_map.onPlayerDeath();
        }
    }

}
