package com.gds.SecondChance.GameObjects.Player;

import com.gds.SecondChance.GameObjects.GameObject;
import com.gds.SecondChance.Map;

/**
 * The corpse of the player. Most interaction with this is done in the player class
 * Created by Adam on 01/09/2014.
 */
public class Corpse extends GameObject {

    public Corpse(Map map, float x, float y) {
        super(map);
        setPosition(x, y);
        setHitbox(0.8f, 0.6f);
    }
}
