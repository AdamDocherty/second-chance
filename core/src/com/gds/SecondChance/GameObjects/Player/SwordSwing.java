package com.gds.SecondChance.GameObjects.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.gds.SecondChance.GameObjects.GameObject;
import com.gds.SecondChance.Map;

/**
 * Created by Adam on 03/09/2014.
 */
public class SwordSwing extends GameObject {
    private boolean m_active;
    private float m_timer;

    private Player m_player;
    private int m_direction;

    public int getDirection() { return m_direction; }

    public SwordSwing(Map map, Player player) {
        super(map);
        setHitbox(1.5f, 2.0f);
        m_player = player;
        m_active = false;
        m_timer = 0.0f;
    }

    public boolean isActive() { return m_active; }

    public void update(float deltaTime) {
        if (!m_active) return;

        m_timer -= deltaTime;

        if (m_timer < 0.0f) {
            m_active = false;
        }
    }

    public void attack(float duration) {
        Sound sfx = m_map.m_assetManager.get("sfx/sword-swing.ogg", Sound.class);
        sfx.play(0.7f);

        m_timer = duration;
        m_active = true;

        m_direction = m_player.getDirection();

        float x = m_player.getX();
        float y = m_player.getY() + 0.5f;

        if (m_direction == Player.Direction.LEFT) {
            x -= m_rect.width;
        } else {
            x += m_player.getHitbox().width;
        }

        setPosition(x, y);
    }
}
