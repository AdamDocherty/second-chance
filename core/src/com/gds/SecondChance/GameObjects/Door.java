package com.gds.SecondChance.GameObjects;

import com.gds.SecondChance.Map;

/**
 * Represents a door which can be opened and closed using the lamps.
 * The door is equal to one tile.
 * Created by Adam on 01/09/2014.
 */
public class Door {
    public enum State {
        OPEN,
        CLOSED
    }

    int m_tileX;
    int m_tileY;

    State m_state;

    String m_label;

    Map m_map;

    public String getLabel() { return m_label; }
    public State getState() { return m_state; }
    public int getX() { return m_tileX; }
    public int getY() { return m_tileY; }

    public Door(Map map, int tileX, int tileY, String label, State state) {
        m_map = map;

        m_tileX = tileX;
        m_tileY = tileY;

        m_label = label;
        m_state = state;

        switch (m_state) {
            case OPEN:
                open();
            case CLOSED:
                close();
        }
    }

    public Door(Map map, int tileX, int tileY, String label) {
        this(map, tileX, tileY, label, State.CLOSED);
    }

    public void open() {
        m_state = State.OPEN;
        m_map.setTile(m_tileX, m_tileY, Map.EMPTY);
    }

    public void close() {
        m_state = State.CLOSED;
        m_map.setTile(m_tileX, m_tileY, Map.DOOR);
    }
}
