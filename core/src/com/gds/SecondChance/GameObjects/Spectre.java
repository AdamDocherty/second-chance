package com.gds.SecondChance.GameObjects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.gds.SecondChance.Map;

/**
 * Created by Adam on 03/09/2014.
 */
public class Spectre extends GameObject {
    public static final float WIDTH = 1.6f;
    public static final float HEIGHT = WIDTH;

    private enum AIState {
        WANDER,
        CHASE,
        FLEE,
        DEAD
    }

    private static final float CHAIN_RANGE = 2.5f; // The furthest distance the spectre
                                                   // will move from it's initial position
    private static final float CHASE_RANGE = 10.0f; // The furthest distance the spectre will chase the player
                                                    // from it's initiali position
    private static final float AGGRO_RANGE = 4.0f; // The distance at which the ghost will start chasing the player
    private static final float SLOW_SPEED = 0.8f; // The speed the spectre will move when wandering and
                                                  // chasing the player in living form
    private static final float FAST_SPEED = 4.0f; // The speed at which the spectre will move when fleeing
                                                  // and chasing the player in ghost form
    private static final float RESPAWN_TIME = 10.0f;

    private Vector2 m_initialPosition;
    private Vector2 m_velocity;

    private AIState m_aiState;
    private float m_aiStateTime;

    public boolean isActive() {
        return m_aiState != AIState.DEAD;
    }

    // Wander variables
    private float m_nextWanderDecision;
    private Vector2 m_wanderTarget = new Vector2();

    public Spectre(Map map, float x, float y) {
        super(map);
        setPosition(x, y);
        setHitbox(WIDTH, HEIGHT);

        m_initialPosition = new Vector2(x, y);
        m_velocity = new Vector2();

        m_aiState = AIState.WANDER;
    }

    public void update(float deltaTime) {
        switch (m_aiState) {
            case WANDER:
                updateWander(deltaTime);
                break;
            case CHASE:
                updateChase(deltaTime);
                break;
            case FLEE:
                updateFlee(deltaTime);
                break;
            case DEAD:
                if (m_aiStateTime > RESPAWN_TIME) {
                    reset();
                }
                break;
        }

        m_aiStateTime += deltaTime;
    }

    private void reset() {
        setPosition(m_initialPosition);
        changeAIState(AIState.WANDER);
    }

    private void changeAIState(AIState state) {
        m_aiState = state;
        m_aiStateTime = 0.0f;
    }

    private void updateWander(float deltaTime) {
        m_nextWanderDecision -= deltaTime;

        if (m_nextWanderDecision < 0.0f) {
            m_nextWanderDecision = 1.0f;
            m_wanderTarget.set(m_initialPosition);
            m_wanderTarget.add((float)Math.random() * (CHAIN_RANGE * 2.0f) - CHAIN_RANGE,
                    (float)Math.random() * (CHAIN_RANGE * 2.0f) - CHAIN_RANGE);
        }

        float angle = (float)Math.atan2(m_wanderTarget.y - m_rect.y, m_wanderTarget.x - m_rect.x);
        m_velocity.x = (float)Math.cos(angle);
        m_velocity.y = (float)Math.sin(angle);

        m_velocity.scl(SLOW_SPEED * deltaTime);

        m_rect.x += m_velocity.x;
        m_rect.y += m_velocity.y;

        if (m_wanderTarget.dst(m_rect.x, m_rect.y) < 0.01f) {
            m_nextWanderDecision = 0.0f;
        }

        Vector2 playerPosition = new Vector2();
        m_map.m_player.m_rect.getPosition(playerPosition);

        if (playerPosition.dst(m_rect.x, m_rect.y) < AGGRO_RANGE &&
            playerPosition.dst(m_initialPosition) < CHASE_RANGE) {
            m_aiState = AIState.CHASE;
        }
    }

    private void updateChase(float deltaTime) {
        Vector2 playerPosition = new Vector2();
        m_map.m_player.m_rect.getPosition(playerPosition);

        float angle = (float)Math.atan2(playerPosition.y - m_rect.y, playerPosition.x - m_rect.x);
        m_velocity.x = (float)Math.cos(angle);
        m_velocity.y = (float)Math.sin(angle);

        m_velocity.scl((m_map.m_player.isGhost() ? FAST_SPEED : SLOW_SPEED) * deltaTime);

        m_rect.x += m_velocity.x;
        m_rect.y += m_velocity.y;

        if (m_initialPosition.dst(m_rect.x, m_rect.y) > CHASE_RANGE) {
            m_aiState = AIState.WANDER;
        }

    }

    private void updateFlee(float deltaTime) {

    }

    public void hitBySword() {
        Sound sfx = m_map.m_assetManager.get("sfx/spectre-death.ogg", Sound.class);
        sfx.play(0.5f);

        changeAIState(AIState.DEAD);
    }

}
