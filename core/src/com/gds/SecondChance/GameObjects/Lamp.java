package com.gds.SecondChance.GameObjects;

import com.gds.SecondChance.Map;

/**
 * Lamp class, a switch that can be turned off when the player moves over it while in ghost form.
 *
 * Created by Adam on 01/09/2014.
 */
public class Lamp extends GameObject {
    public enum State {
        LIT,
        UNLIT
    }

    String m_label;

    State m_state;

    public State getState() { return m_state; }

    public Lamp(Map map, float x, float y, String label) {
        super(map);

        setPosition(x, y);
        setHitbox(1.0f, 1.0f);

        m_label = label;
        m_state = State.UNLIT;
    }

    public void activate() {
        m_state = State.LIT;

        m_map.toggleDoors(m_label);
    }
}
