package com.gds.SecondChance.GameObjects;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gds.SecondChance.Map;

/**
 * Base class for all game objects.
 *
 * Game objects have a rect which stores the position (x, y) and bounding box information
 *
 * Created by Adam on 02/09/2014.
 */
public class GameObject {
    protected Rectangle m_rect = new Rectangle();
    protected Map m_map;

    public GameObject(Map map) {
        m_map = map;
    }

    public Rectangle getHitbox() {
        return m_rect;
    }

    public void setPosition(Vector2 pos) {
        m_rect.x = pos.x;
        m_rect.y = pos.y;
    }

    public void setPosition(float x, float y) {
        m_rect.x = x;
        m_rect.y = y;
    }

    public void setX(float x) {
        m_rect.x = x;
    }
    public void setY(float y) {
        m_rect.y = y;
    }

    public float getX() { return m_rect.x; }
    public float getY() { return m_rect.y; }

    public void setHitbox(float width, float height) {
        m_rect.width = width;
        m_rect.height = height;
    }

    public void renderShape(ShapeRenderer shapeRenderer) {
        shapeRenderer.rect(m_rect.x, m_rect.y, m_rect.width, m_rect.height);
    }


}
