package com.gds.SecondChance.GameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.gds.SecondChance.GameObjects.Player.Player;
import com.gds.SecondChance.GameObjects.Player.SwordSwing;
import com.gds.SecondChance.Map;

/**
 * Created by Adam on 05/09/2014.
 */
public class Boss extends GameObject {


    public enum Phase {
        PHASE_1,
        PHASE_2,
        PHASE_3,
        PHASE_4
    }

    public static class BossProjectile extends GameObject {
        private static final float SPEED = 10.0f;
        private static final float DURATION = 2.0f;

        private float m_timeRemaining;

        private Vector2 m_velocity;
        private boolean isActive() { return m_timeRemaining > 0.0f; }

        public BossProjectile(Map map) {
            super(map);
            m_timeRemaining = -1.0f;
            setHitbox(0.5f, 0.5f);
            m_velocity = new Vector2();
        }

        public void update(float deltaTime) {
            m_rect.x += m_velocity.x * deltaTime;
            m_rect.y += m_velocity.y * deltaTime;

            m_timeRemaining -= deltaTime;
        }

        public void fire(float x, float y) {
            setPosition(m_map.m_boss.getX() + 3.0f, m_map.m_boss.getY() + 3.0f);
            m_velocity.set(x, y);
            m_velocity.nor();
            m_velocity.scl(SPEED);
            m_timeRemaining = DURATION;
        }
    }

    public static final int MAX_HEALTH = 50;

    private static final int PHASE_2_TRANSITION = 37;
    private static final int PHASE_3_TRANSITION = 25;
    private static final int PHASE_4_TRANSITION = 12;

    private static final float MAX_LEFT = 2;
    private static final float MAX_RIGHT = 30;

    private static final float WIDTH = 6;
    private static final float HEIGHT = 6;

    public BossProjectile m_projectile = null;

    private float m_speed = 5.0f;
    private Vector2 m_velocity = new Vector2();

    private int m_xDirection; // 1 for right, -1 for left
    private int m_yDirection; // 1 for up, -1 for down, 0 for not moving

    private int m_health;

    public int getHealth() { return m_health; }

    private Phase m_phase;

    public Phase getPhase() { return m_phase; }

    public Boss(Map map) {
        super(map);

        setPosition(MAX_RIGHT, 2);
        setHitbox(WIDTH, HEIGHT);

        m_xDirection = -1;
        m_yDirection = 0;

        m_health = MAX_HEALTH;

        m_phase = Phase.PHASE_1;
    }

    public void update(float deltaTime) {
        m_velocity.set(m_xDirection, m_yDirection);
        m_velocity.nor();
        m_velocity.scl(m_speed * deltaTime);

        m_rect.x += m_velocity.x;
        m_rect.y += m_velocity.y;

        if (m_rect.x < MAX_LEFT) {
            m_xDirection = 1;
        } else if (m_rect.x + m_rect.width > MAX_RIGHT) {
            m_xDirection = -1;
        }

        if (m_phase != Phase.PHASE_1) {
            if (m_rect.y < 2) {
                m_yDirection = 1;
            } else if (m_rect.y + m_rect.height > 14) {
                m_yDirection = -1;
            }
        }

        if (m_phase == Phase.PHASE_4) {
            if (!m_projectile.isActive()) {
                float angle = (float)Math.atan2(m_map.m_player.getY() - (m_rect.y + 3.5f),
                        m_map.m_player.getX() - (m_rect.x + 3));
                m_projectile.fire((float)Math.cos(angle), (float)Math.sin(angle));
            }
            m_projectile.update(deltaTime);
        }
    }

    public boolean isColliding(Player player) {
        return isCollidingWithAll(player);
    }

    public boolean isColliding(SwordSwing ss) {
        return isCollidingWithAll(ss);
    }

    public void onHit() {
        m_health -= 1;
        if (m_health == PHASE_2_TRANSITION) {
            m_phase = Phase.PHASE_2;
            m_yDirection = 1;
        } else if (m_health == PHASE_3_TRANSITION) {
            m_phase = Phase.PHASE_3;
            m_speed *= 1.5f;
        } else if (m_health == PHASE_4_TRANSITION) {
            m_phase = Phase.PHASE_4;
            m_projectile = new BossProjectile(m_map);
        } else if (m_health <= 0) {
            m_map.onLevelComplete();
        }
    }

    private boolean isCollidingWithAll(GameObject o) {
        if (m_rect.overlaps(o.getHitbox())) return true;
        if (m_projectile != null && m_projectile.getHitbox().overlaps(o.getHitbox())) return true;
        return false;
    }
}
