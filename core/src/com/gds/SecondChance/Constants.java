package com.gds.SecondChance;

/**
 * Class containing all constants used in the game
 * Created by Adam on 02/09/2014.
 */
public class Constants {
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 800 * 9 / 16;
    public static final float ASPECT_RATIO = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;

    public static final String PACKED_TEXTURE_DIR = "textures/packed-textures/";

    public static final float MAP_SCALE = 32.0f;

    public static String getPackedTexture(String atlas) {
        return PACKED_TEXTURE_DIR + atlas + ".atlas";
    }
}
