package com.gds.SecondChance;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.gds.SecondChance.GameObjects.*;
import com.gds.SecondChance.GameObjects.Player.Player;
import com.gds.SecondChance.GameObjects.Player.SwordSwing;

/**
 * Class for rendering the Map.
 *
 * All textures and animations for the game screen are stored in this class.
 *
 * Created by Adam on 01/09/2014.
 */
public class MapRenderer {
    private static final String TAG = "MapRenderer";

    private Map m_map;
    private OrthographicCamera m_orthoCamera;
    private SpriteCache m_cache;
    private Batch m_batch;
    private ShapeRenderer m_shapeRenderer = new ShapeRenderer();
    private int[][] m_blocks;

    private OrthographicCamera m_tiledMapCamera;
    private OrthogonalTiledMapRenderer m_tiledRenderer;
    private int[] m_background = new int[] {0};

    private OrthographicCamera m_uiCamera;

    private TextureRegion m_tile;
    private TextureRegion m_ghostTile;
    private TextureRegion m_spikes;

    // Player Animations
//    Animation m_playerLeft;
    private Animation m_playerAnim;
//    Animation m_playerJumpRight;
//    Animation m_playerJumpLeft;
//    Animation m_playerIdleLeft;
//    Animation m_playerIdleRight;
//    Animation m_playerDead;
//    Animation m_playerSpawn;
//    Animation m_playerDying;

    private Animation m_swordSwingAnim;

    private Animation m_ghostAnim;
//    Animation m_ghostLeft;

    private Animation m_spectreAnim;

    private FPSLogger fps = new FPSLogger();

    public MapRenderer(Map map) {
        m_map = map;
        final float cameraHeight = 16.0f * 0.75f;
        m_orthoCamera = new OrthographicCamera(cameraHeight * Constants.ASPECT_RATIO, cameraHeight);
        m_orthoCamera.position.set(m_map.m_player.getHitbox().x, m_map.m_player.getHitbox().y, 0.0f);
        m_cache = new SpriteCache(m_map.width() * m_map.height(), false);
        m_blocks = new int[(int)Math.ceil(m_map.width() / 24.0f)]
                [(int)Math.ceil(m_map.height() / 16.0f)];

        if (m_map.m_tiledMap != null) {
            m_tiledMapCamera = new OrthographicCamera(cameraHeight * Constants.ASPECT_RATIO * Constants.MAP_SCALE,
                    cameraHeight * Constants.MAP_SCALE);
            m_tiledRenderer = new OrthogonalTiledMapRenderer(m_map.m_tiledMap);
            m_batch = m_tiledRenderer.getSpriteBatch();
        } else {
            m_batch = new SpriteBatch(1000);
        }

        m_uiCamera = new OrthographicCamera(1.0f, 1.0f);
        m_uiCamera.update();

        loadTextures();
        createBlocks();
    }

    private void createBlocks() {
        int width = m_map.width();
        int height = m_map.height();
        for (int blockY = 0; blockY < m_blocks[0].length; ++blockY) {
            for (int blockX = 0; blockX < m_blocks.length; blockX++) {
                m_cache.beginCache();
                for (int y = blockY * 16; y < blockY * 16 + 16; y++) {
                    for (int x = blockX * 24; x < blockX * 24 + 24; x++) {
                        if (x >= width) continue;
                        if (y >= height) continue;
                        int posY = height - y - 1;
                        if (m_map.getTile(x, y) == Map.TILE) m_cache.add(m_tile, x, posY, 1, 1);
                        if (m_map.getTile(x, y) == Map.GHOST_TILE) m_cache.add(m_ghostTile, x, posY, 1, 1);
                        if (m_map.getTile(x, y) == Map.SPIKE) m_cache.add(m_spikes, x, posY, 1, 1);
                    }
                }
                m_blocks[blockX][blockY] = m_cache.endCache();
            }
        }
    }

    private void loadTextures() {
        m_tile = new TextureRegion(new Texture(Gdx.files.internal("images/tile.png")), 0, 0, 20, 20);
        m_ghostTile = new TextureRegion(new Texture(Gdx.files.internal("images/tile.png")), 10, 10, 30, 30);
        Texture bobTexture = new Texture(Gdx.files.internal("images/bob.png"));
        TextureRegion[] split = new TextureRegion(bobTexture).split(20, 20)[0];
        TextureRegion[] mirror = new TextureRegion(bobTexture).split(20, 20)[0];
        for (TextureRegion region : mirror)
            region.flip(true, false);
        m_spikes = split[5];
//        m_playerAnim = new Animation(0.1f, split[0], split[1]);
//        m_playerLeft = new Animation(0.1f, mirror[0], mirror[1]);
//        m_playerJumpRight = new Animation(0.1f, split[2], split[3]);
//        m_playerJumpLeft = new Animation(0.1f, mirror[2], mirror[3]);
//        m_playerIdleRight = new Animation(0.5f, split[0], split[4]);
//        m_playerIdleLeft = new Animation(0.5f, mirror[0], mirror[4]);
//        m_playerDead = new Animation(0.2f, split[0]);
//        split = new TextureRegion(bobTexture).split(20, 20)[2];
//        m_playerSpawn = new Animation(0.1f, split[4], split[3], split[2], split[1]);
//        m_playerDying = new Animation(0.1f, split[1], split[2], split[3], split[4]);

        TextureAtlas playerAtlas = new TextureAtlas(Gdx.files.internal(Constants.getPackedTexture("player")));
        m_ghostAnim = new Animation(0.1f, playerAtlas.findRegions("ghost"));
        m_playerAnim = new Animation(0.1f, playerAtlas.findRegions("idle"));
        m_swordSwingAnim = new Animation(0.1f, playerAtlas.findRegions("sword-swing"));

        TextureAtlas enemyAtlas = new TextureAtlas(Gdx.files.internal(Constants.getPackedTexture("enemies")));
        m_spectreAnim = new Animation(0.1f, enemyAtlas.findRegions("spectre"));
    }

    float m_stateTime = 0;
    Vector3 lerpTarget = new Vector3();

    public void render (float deltaTime) {
        m_orthoCamera.position.lerp(lerpTarget.set(m_map.m_player.getHitbox().x, m_map.m_player.getHitbox().y, 0),
                10.0f * deltaTime);
        // lock the camera position to inside of the level
        if (m_orthoCamera.position.y - m_orthoCamera.viewportHeight * 0.5f < 0.0f) {
            m_orthoCamera.position.y = 0.0f + m_orthoCamera.viewportHeight * 0.5f;
        } else if (m_orthoCamera.position.y + m_orthoCamera.viewportHeight * 0.5f > m_map.height()) {
            m_orthoCamera.position.y = m_map.height() - m_orthoCamera.viewportHeight * 0.5f;
        }
        if (m_orthoCamera.position.x - m_orthoCamera.viewportWidth * 0.5f < 0.0f) {
            m_orthoCamera.position.x = 0.0f + m_orthoCamera.viewportWidth * 0.5f;
        } else if (m_orthoCamera.position.x + m_orthoCamera.viewportWidth * 0.5f > m_map.width()) {
            m_orthoCamera.position.x = m_map.width() - m_orthoCamera.viewportWidth * 0.5f;
        }
        m_orthoCamera.update();
        m_tiledMapCamera.position.set(m_orthoCamera.position);
        m_tiledMapCamera.position.scl(Constants.MAP_SCALE);
        m_tiledMapCamera.update();

        m_cache.setProjectionMatrix(m_orthoCamera.combined);
        Gdx.gl.glDisable(GL20.GL_BLEND);
        m_cache.begin();
        for (int blockY = 0; blockY < m_blocks[0].length; ++blockY) {
            for (int[] m_block : m_blocks) {
                m_cache.draw(m_block[blockY]);
            }
        }
        m_cache.end();

        m_shapeRenderer.setProjectionMatrix(m_orthoCamera.combined);

        m_stateTime += deltaTime;

        if (m_tiledRenderer != null) {
            m_tiledRenderer.setView(m_tiledMapCamera);
            m_tiledRenderer.render(m_background);
            m_tiledRenderer.setView(m_orthoCamera);

            renderBoss();

            m_batch.begin();
            renderPlayer();
            renderEnemies();
            m_batch.end();
        } else {
            m_batch.setProjectionMatrix(m_orthoCamera.combined);
            renderBoss();

            m_batch.begin();
            renderPlayer();
            renderEnemies();
            m_batch.end();
        }


        /// Stuff rendered with the shape renderer
        //renderPlayerHitbox();
        renderCorpse();
        //renderLamps();
        //renderDoors();

        /// Render UI stuff
        m_shapeRenderer.setProjectionMatrix(m_uiCamera.combined);
        renderBossHealth();

        fps.log();
    }

    private void renderPlayer() {
        Animation anim = null;
        boolean loop = true;
//        if (m_map.m_player.getState() == Player.State.RUN) {
//            if (m_map.m_player.getDirection() == Player.Direction.LEFT)
//                anim = m_playerLeft;
//            else
//                anim = m_playerAnim;
//        }
//        if (m_map.m_player.getState() == Player.State.IDLE) {
//            if (m_map.m_player.getDirection() == Player.Direction.LEFT)
//                anim = m_playerIdleLeft;
//            else
//                anim = m_playerIdleRight;
//        }
//        if (m_map.m_player.getState() == Player.State.JUMP) {
//            if (m_map.m_player.getDirection() == Player.Direction.LEFT)
//                anim = m_playerJumpLeft;
//            else
//                anim = m_playerJumpRight;
//        }
//        if (m_map.m_player.getState() == Player.State.SPAWN) {
//            anim = m_playerSpawn;
//            loop = false;
//        }
//        if (m_map.m_player.getState() == Player.State.DYING) {
//            anim = m_playerDying;
//            loop = false;
//        }

        Player player = m_map.m_player;
        TextureRegion region = null;

        float x = player.getX();
        float y = player.getY();
        float width = 1.0f;
        float height = 1.0f;

        if (player.isGhost()) {
            anim = m_ghostAnim;
            region = anim.getKeyFrame(player.getStateTime());

            y += 0.2f;

            width = 0.7f;
            height = width * region.getRegionHeight() / region.getRegionWidth();
        } else {
            anim = m_playerAnim;
            region = anim.getKeyFrame(player.getStateTime());

            x -= 0.2f;
            y -= 0.2f;
            height = width * region.getRegionHeight() / region.getRegionWidth();
        }

        if (null == anim || null == region) {
            Gdx.app.error(TAG, "Could not find the correct animation to render the player.");
        } else {
            int direction = player.getDirection();
            if (direction == Player.Direction.LEFT) x = x + width;

            m_batch.draw(region, x, y, width * direction, height);
            if (player.m_swordSwing.isActive()) {
                SwordSwing ss = player.m_swordSwing;
                int ssDir = ss.getDirection();

                m_batch.draw(m_swordSwingAnim.getKeyFrame(0.0f),
                        (ssDir == 1) ? ss.getX() : ss.getX() + ss.getHitbox().width, ss.getY(),
                        ss.getHitbox().width * ssDir, ss.getHitbox().height);
            }
        }
    }

    private void renderBoss() {
        if (m_map.m_boss == null) return;

        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        m_shapeRenderer.setColor(Color.RED);
        m_map.m_boss.renderShape(m_shapeRenderer);
        if (m_map.m_boss.m_projectile != null) {
            m_shapeRenderer.setColor(Color.PURPLE);
            m_map.m_boss.m_projectile.renderShape(m_shapeRenderer);
        }
        m_shapeRenderer.end();
    }

    private void renderBossHealth() {
        if (m_map.m_boss == null) return;

        int maxHealth = Boss.MAX_HEALTH;
        int health = m_map.m_boss.getHealth();
        Boss.Phase phase = m_map.m_boss.getPhase();

        Color color;

        float percentage = (float)health / (float)maxHealth;
        switch (phase) {
            case PHASE_1:
                color = new Color(Color.GREEN);
                break;
            case PHASE_2:
                color = new Color(Color.YELLOW);
                break;
            case PHASE_3:
                color = new Color(Color.ORANGE);
                break;
            case PHASE_4:
                color = new Color(Color.RED);
                break;
            default:
                color = new Color(Color.WHITE);
        }
        color.a = 0.5f;

        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        m_shapeRenderer.setColor(color);
        m_shapeRenderer.rect(-0.45f, 0.4f, 0.9f * percentage, 0.05f);
        m_shapeRenderer.end();
    }

    private void renderPlayerHitbox() {
        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        m_shapeRenderer.setColor(Color.RED);
        m_map.m_player.renderShape(m_shapeRenderer);
        m_shapeRenderer.end();
    }

    private void renderEnemies() {
       if (m_map.m_spectres.size > 0) {
           for (Spectre o : m_map.m_spectres) {
               if (o.isActive()) {
                   m_batch.draw(m_spectreAnim.getKeyFrame(0.0f),
                           o.getHitbox().getX(), o.getHitbox().y, Spectre.WIDTH, Spectre.HEIGHT);
               }
           }
       }
    }

    private void renderCorpse() {
        if (null == m_map.m_player.m_corpse) return;

        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        m_shapeRenderer.setColor(Color.LIGHT_GRAY);
        m_map.m_player.m_corpse.renderShape(m_shapeRenderer);
        m_shapeRenderer.end();
    }

    private void renderLamps() {
        if (m_map.m_lamps.size == 0) return;

        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        for (Lamp lamp : m_map.m_lamps) {
            if (lamp.getState() == Lamp.State.LIT)
                m_shapeRenderer.setColor(Color.ORANGE);
            else
                m_shapeRenderer.setColor(Color.DARK_GRAY);
            lamp.renderShape(m_shapeRenderer);
        }
        m_shapeRenderer.end();
    }

    private void renderDoors() {
        if (m_map.m_doors.size == 0) return;

        m_shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        m_shapeRenderer.setColor(0.5f, 0.5f, 0.2f, 1.0f);
        for (Door door : m_map.m_doors) {
            if (door.getState() == Door.State.OPEN)
                continue;
            m_shapeRenderer.rect(door.getX(), m_map.height() - door.getY() - 1, 1.0f, 1.0f);
        }
        m_shapeRenderer.end();
    }

    public void dispose() {
        m_cache.dispose();
        m_batch.dispose();
        m_tile.getTexture().dispose();
    }

}
