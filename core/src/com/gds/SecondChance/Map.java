package com.gds.SecondChance;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.gds.SecondChance.GameObjects.Boss;
import com.gds.SecondChance.GameObjects.Door;
import com.gds.SecondChance.GameObjects.Lamp;
import com.gds.SecondChance.GameObjects.Player.Player;
import com.gds.SecondChance.GameObjects.Spectre;

/**
 * Class containing all Game Objects and tile map elements
 * Created by Adam on 01/09/2014.
 */
public class Map {

    public enum State {
        NOT_LOADED,
        RUNNING,
        PLAYER_DEAD,
        LEVEL_COMPLETE
    }

    private static final String TAG = "Map";

    public static final byte EMPTY = 0;
    public static final byte TILE = 1;
    public static final byte GHOST_TILE = 2;
    public static final byte SPIKE = 3;
    public static final byte END = 4;
    public static final byte DOOR = 5;

    public AssetManager m_assetManager;

    private State m_state;
    public State getState() { return m_state; }

    public TiledMap m_tiledMap;

    private byte[][] m_tiles;
    public Player m_player;
    public Boss m_boss;
    public Array<Lamp> m_lamps = new Array<Lamp>();
    public Array<Door> m_doors = new Array<Door>();
    public Array<Spectre> m_spectres = new Array<Spectre>();

    private Vector2 m_playerSpawnPosition;

    public Map(String levelName) {
        m_assetManager = new AssetManager();
        m_assetManager.load("sfx/sword-swing.ogg", Sound.class);
        m_assetManager.load("sfx/spectre-death.ogg", Sound.class);
        m_assetManager.load("sfx/player-death.ogg", Sound.class);
        m_assetManager.load("sfx/boss-death.ogg", Sound.class);

        while (!m_assetManager.update()){

        }

        m_state = State.NOT_LOADED;

        //loadBinary("levels/level.png");
        loadTMX(levelName);
    }

    public int width() { return m_tiles.length; }
    public int height() { return m_tiles[0].length; }
    public int getTile(int x, int y) {
        if (x < 0 || x >= m_tiles.length) return EMPTY;
        if (y < 0 || y >= m_tiles[0].length) return EMPTY;
        return m_tiles[x][y];
    }
    public void setTile(int x, int y, byte tileId) { m_tiles[x][y] = tileId; }

    private void loadDefault() {
        m_tiles = new byte[32][32];
        for (int i = 0; i < 32; ++i) {
            m_tiles[i][0] = TILE;
            m_tiles[i][31] = TILE;
            m_tiles[0][i] = TILE;
            m_tiles[31][i] = TILE;
        }

        for (int i = 0; i < 5; ++i) {
            m_tiles[1+i][25+i] = GHOST_TILE;
        }

        m_tiles[15][30] = SPIKE;

        m_playerSpawnPosition = new Vector2(10.0f, 5.0f);
        m_player = new Player(this, m_playerSpawnPosition.x, m_playerSpawnPosition.y);
        m_lamps.add(new Lamp(this, 1.0f, 1.0f, "default"));
        m_doors.add(new Door(this, 5, 30, "default"));
        m_spectres.add(new Spectre(this, 5.0f, 5.0f));

        m_state = State.RUNNING;
    }

    private void loadTMX(String filename) {
        m_tiledMap = new TmxMapLoader().load(filename);
        TiledMapTileLayer collisionLayer = (TiledMapTileLayer) m_tiledMap.getLayers().get("Collisions");

        int width = collisionLayer.getWidth();
        int height = collisionLayer.getHeight();

        m_tiles = new byte[width][height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                TiledMapTileLayer.Cell c = collisionLayer.getCell(x, y);
                if (c != null) {
                    int id = c.getTile().getId();
                    switch (id) {
                        case 257:
                            m_tiles[x][height - 1 - y] = EMPTY;
                            break;
                        case 257 + 1:
                            m_tiles[x][height - 1 - y] = TILE;
                            break;
                        case 257 + 2:
                            m_tiles[x][height - 1 - y] = SPIKE;
                            break;
                        case 257 + 3:
                            m_tiles[x][height - 1 - y] = END;
                            break;
                    }
                }
            }
        }

        Ellipse playerStart =  ((EllipseMapObject)m_tiledMap.getLayers().
                get("PlayerStart").getObjects().get(0)).getEllipse();
        m_playerSpawnPosition = new Vector2(playerStart.x, playerStart.y);
        m_playerSpawnPosition.scl(1.0f/Constants.MAP_SCALE);

        m_player = new Player(this, m_playerSpawnPosition.x, m_playerSpawnPosition.y);

        MapLayer spectreLayer = m_tiledMap.getLayers().get("Spectre");
        if (spectreLayer != null) {
            for (MapObject object : spectreLayer.getObjects()) {
                if (object instanceof EllipseMapObject) {
                    Ellipse e = ((EllipseMapObject) object).getEllipse();
                    m_spectres.add(new Spectre(this, e.x / Constants.MAP_SCALE, e.y / Constants.MAP_SCALE));
                }
            }
        }

        if (filename.contains("boss")) {
            m_boss = new Boss(this);
        }

        m_state = State.RUNNING;
    }

    public void update(float deltaTime) {

        switch (m_state) {
            case RUNNING:
                m_player.update(deltaTime);
                if (m_player.getState() == Player.State.DEAD) {
                    m_player = new Player(this, m_playerSpawnPosition.x, m_playerSpawnPosition.y);
                }

                for (Spectre s : m_spectres) {
                    s.update(deltaTime);
                }
                if (m_boss != null) {
                    m_boss.update(deltaTime);
                }
                break;
            default:
                break;
        }

    }

    public boolean isCollidable (int tileId, boolean isGhost) {
        if (isGhost) {
            return tileId == TILE;
        } else {
            return (tileId == TILE) || (tileId == DOOR) || (tileId == GHOST_TILE);
        }
    }

    public boolean isEnd(int tileId) {
        return tileId == END;
    }

    public boolean isDeadly (int tileId) {
        return tileId == SPIKE;
    }

    public Vector2 getPlayerSpawnPosition() {
        return m_playerSpawnPosition;
    }

    public void toggleDoors(String label) {
        Gdx.app.log(TAG, "Lamp activated with label " + label + ".");
        int doorsOpened = 0;
        for (Door door : m_doors) {
            if (door.getLabel().contentEquals(label)) {
                door.open();
                doorsOpened++;
            }
        }
        Gdx.app.log(TAG, doorsOpened + " doors opened.");
    }

    public void onPlayerDeath() {
        m_state = State.PLAYER_DEAD;
    }


    public void onLevelComplete() {
        m_state = State.LEVEL_COMPLETE;
    }
}
