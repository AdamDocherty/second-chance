package com.gds.SecondChance.Screens;

import com.badlogic.gdx.Screen;
import com.gds.SecondChance.SecondChance;

/**
 * Created by Adam on 03/09/2014.
 */
public class AbstractScreen implements Screen {
    protected SecondChance m_game;

    public AbstractScreen(SecondChance m_game) {
        this.m_game = m_game;
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
