package com.gds.SecondChance.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gds.SecondChance.SecondChance;

/**
 * Created by Adam on 03/09/2014.
 */
public class WinScreen extends AbstractScreen {
    private enum State {
        DEFAULT,
        FADE_OUT
    }

    private static final float FADE_OUT_TIME = 1.0f;

    private State m_state = State.DEFAULT;

    private TextureRegion m_title;
    private SpriteBatch m_batch;

    private float m_time;

    private Music m_music;

    public WinScreen(SecondChance m_game) {
        super(m_game);
    }

    @Override
    public void show() {
        super.show();
        m_title = new TextureRegion(new Texture(Gdx.files.internal("images/winScreen.png")));
        m_batch = new SpriteBatch();
        m_batch.getProjectionMatrix().setToOrtho2D(0, 0, m_title.getRegionWidth(), m_title.getRegionHeight());

        m_music = Gdx.audio.newMusic(Gdx.files.internal("music/title-music.mp3"));
        m_music.play();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        m_batch.begin();

        if (m_state == State.FADE_OUT) {
            m_batch.setColor(1.0f, 1.0f, 1.0f, 1.0f - (m_time / FADE_OUT_TIME));
        }

        m_batch.draw(m_title, 0, 0);
        m_batch.end();

        m_time += delta;

        switch (m_state) {
            case DEFAULT:
                if (m_time > 1.0f) {
                    boolean buttonPressed = Gdx.input.isKeyPressed(Input.Keys.ANY_KEY);
                    Controller c = Controllers.getControllers().get(0);
                    for (int i = 0; i < 10; ++i) {
                        if (c.getButton(i)) buttonPressed = true;
                    }
                    if (buttonPressed) {
                        m_state = State.FADE_OUT;
                        m_time = 0.0f;
                    }
                }
                break;
            case FADE_OUT:
                m_music.setVolume(Math.max(0.0f, 1.0f - (m_time / FADE_OUT_TIME)));
                if (m_time > FADE_OUT_TIME) {
                    startGame();
                }
                break;
        }


    }

    private void startGame() {
        m_game.setScreen(new GameScreen(m_game));
    }

    @Override
    public void dispose() {
        super.dispose();
        if (null != m_music) {
            m_music.stop();
            m_music.dispose();
        }
    }
}
