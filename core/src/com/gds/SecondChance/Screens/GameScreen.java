package com.gds.SecondChance.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.gds.SecondChance.Map;
import com.gds.SecondChance.MapRenderer;
import com.gds.SecondChance.SecondChance;

/**
 * Game Screen class, contains the map and renderer for the main game.
 *
 * Created by Adam on 01/09/2014.
 */
public class GameScreen extends AbstractScreen {

    private Map m_map;
    private MapRenderer m_renderer;

    private Music m_levelMusic;

    private static final String LEVEL_1_NAME = "levels/level01.tmx";
    private static final String LEVEL_BOSS_NAME = "levels/boss01.tmx";

    private static String s_currentLevel = LEVEL_1_NAME;

    private String m_levelName;

    public GameScreen(SecondChance game) {
        super(game);
        m_levelName = s_currentLevel;
    }

    @Override
    public void render(float delta) {
        delta = Math.min(0.06f, Gdx.graphics.getDeltaTime());

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            m_game.setScreen(new TitleScreen(m_game));
            return;
        }

        if (m_map.getState() == Map.State.PLAYER_DEAD) {
            m_game.setScreen(new TitleScreen(m_game));
            return;
        }

        if (m_map.getState() == Map.State.LEVEL_COMPLETE) {
            if (m_levelName.contentEquals(LEVEL_1_NAME)) {
                m_renderer.dispose();
                m_levelMusic.dispose();

                s_currentLevel = LEVEL_BOSS_NAME;
                m_levelName = LEVEL_BOSS_NAME;
                m_map = new Map(m_levelName);
                m_renderer = new MapRenderer(m_map);

                m_levelMusic = Gdx.audio.newMusic(Gdx.files.internal("music/boss-music.mp3"));
                m_levelMusic.setLooping(true);
                m_levelMusic.setVolume(0.7f);
                m_levelMusic.play();
            } else {
                m_game.setScreen(new WinScreen(m_game));
            }
            return;
        }

        m_map.update(delta);

        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        m_renderer.render(delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        m_map = new Map(m_levelName);
        m_renderer = new MapRenderer(m_map);

        if (m_levelName == LEVEL_BOSS_NAME) {
            m_levelMusic = Gdx.audio.newMusic(Gdx.files.internal("music/boss-music.mp3"));
            m_levelMusic.setLooping(true);
            m_levelMusic.setVolume(0.7f);
            m_levelMusic.play();
        } else {
            m_levelMusic = Gdx.audio.newMusic(Gdx.files.internal("music/level-music.mp3"));
            m_levelMusic.setLooping(true);
            m_levelMusic.setVolume(0.5f);
            m_levelMusic.play();
        }
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        m_map.m_assetManager.dispose();
        m_renderer.dispose();
        m_levelMusic.dispose();
    }
}
