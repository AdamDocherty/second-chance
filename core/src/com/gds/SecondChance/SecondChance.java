package com.gds.SecondChance;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gds.SecondChance.Screens.GameScreen;
import com.gds.SecondChance.Screens.TitleScreen;

public class SecondChance extends Game {
	@Override
	public void create () {

        setScreen(new TitleScreen(this));
	}

    @Override
    public void setScreen(Screen screen) {
        Screen oldScreen = getScreen();

        if (null != oldScreen)
            oldScreen.dispose();

        super.setScreen(screen);

    }
}
