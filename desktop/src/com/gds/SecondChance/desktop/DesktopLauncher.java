package com.gds.SecondChance.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gds.SecondChance.Constants;
import com.gds.SecondChance.SecondChance;

public class DesktopLauncher {
	public static void main (String[] arg) {
        ImagePacker.run();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Second Chance";
        config.width = Constants.SCREEN_WIDTH;
        config.height = Constants.SCREEN_HEIGHT;
        new LwjglApplication(new SecondChance(), config);
	}
}
