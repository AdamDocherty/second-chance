package com.gds.SecondChance.desktop;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.gds.SecondChance.Constants;

/**
 * Created by Adam on 02/09/2014.
 */
public class ImagePacker {
    public static void run() {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.filterMin = Texture.TextureFilter.Linear;
        settings.filterMag = Texture.TextureFilter.Linear;
        settings.pot = false;

        process(settings, "player");
        process(settings, "enemies");
    }

    private static void process(TexturePacker.Settings settings, String dir) {
        TexturePacker.process(settings, "textures/" + dir, Constants.PACKED_TEXTURE_DIR, dir);
    }
}
